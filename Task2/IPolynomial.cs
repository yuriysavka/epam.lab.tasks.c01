
public interface IPolynomial<T>
{
    int Degree { get; }
    T[] Coefficients { get; }
    
    T Evaluate(T x);
    IPolynomial<T> Add(IPolynomial<T> polynomial);
    IPolynomial<T> Subtract(IPolynomial<T> polynomial);
    IPolynomial<T> Multiply(IPolynomial<T> polynomial);
}