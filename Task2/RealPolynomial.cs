
using System;
using System.Text;

class RealPolynomial : IPolynomial<double>
{
    public double[] Coefficients { get; private set; }

    public int Degree
    {
        get
        {
            return Coefficients.Length - 1;
        }
    }

    public RealPolynomial()
    { 
        Coefficients = new double[] {0};
    }

    public RealPolynomial(double[] coefficientsFromHighToLow )
    {
        this.Coefficients = ReduceLeadingZeroCoefficients(coefficientsFromHighToLow);
    }

    public RealPolynomial(string polynomialString)
    {
        this.Coefficients = ParseCoefficients(polynomialString);
    }

    public static RealPolynomial Parse(string polynomialString)
    {
        return new RealPolynomial(ParseCoefficients(polynomialString));
    }

    public double Evaluate(double x)
    {
        double value = 0;

        // Use Horner's method for evaluation.
        for (int i = 0; i <= Degree; i++)
        {
            value = Coefficients[i] + x * value;
        }
        return value;
    }

    public IPolynomial<double> Add(IPolynomial<double> polynomial)
    {
        var resultCoefficients = new double[Math.Max(Degree, polynomial.Degree) + 1];
        int index;

        for (int i = 0; i <= Degree; i++)
        {
            index = i + resultCoefficients.Length - Degree - 1;
            resultCoefficients[index] += this.Coefficients[i];
        }
        for (int i = 0; i <= polynomial.Degree; i++)
        {
            index = i + resultCoefficients.Length - polynomial.Degree - 1;
            resultCoefficients[index] += polynomial.Coefficients[i];
        }
        return new RealPolynomial(ReduceLeadingZeroCoefficients(resultCoefficients));
    }

    public IPolynomial<double> Multiply(IPolynomial<double> p)
    {
        IPolynomial<double> result = new RealPolynomial();
        double[] resultCoefficients;

        for (int i = 0; i <= Degree; i++)
        {
            for (int j = 0; j <= p.Degree; j++)
            {
                resultCoefficients = new double[i + j + 1];
                resultCoefficients[0] = Coefficients[Degree - i] * p.Coefficients[p.Degree - j];
                result = result.Add(new RealPolynomial(resultCoefficients));
            }
        }
        return result;
    }


    public IPolynomial<double> Subtract(IPolynomial<double> p)
    {
        return Add(p.Multiply(new RealPolynomial(new double[] { -1 })));
    }

    public override string ToString()
    {
        StringBuilder polynomialString = new StringBuilder();
        string termCoefficient;
        for (int i = 0; i < Coefficients.Length; i++)
        {
            if (Coefficients[i] != 0)
            {
                termCoefficient = Coefficients[i] == 1 ? "" : Coefficients[i].ToString();
                if (i < Coefficients.Length - 2)
                {
                    polynomialString.Append(termCoefficient + "x^" +
                        (Degree - i) + "+" );
                }
                else if (i == Coefficients.Length - 2)
                {
                    polynomialString.Append(termCoefficient + "x+");
                }
                else
                {
                    polynomialString.Append(Coefficients[i]);
                }
            }
        }

        int index = polynomialString.Length - 1;

        if ((index >= 0) && polynomialString[index] == '+')
        {
            polynomialString.Remove(polynomialString.Length - 1, 1);
        }
        return polynomialString.Length > 0 ?
            polynomialString.Replace("+-", "-").ToString() : "0";
    }

    public override bool Equals(object obj)
    {
        var polynomial = obj as RealPolynomial;

        if ((polynomial == null) || (polynomial.Degree != Degree))
        {
            return false;
        }

        for (int i = 0; i <= Degree; i++)
        {
            if (Coefficients[i] != polynomial.Coefficients[i])
            {
                return false;
            }
        }

        return true;
    }

    public override int GetHashCode()
    {
        return this.ToString().GetHashCode();
    }

    #region Helpers

    private static double[] ParseCoefficients(string polynomialString)
    {
        string[] terms = polynomialString.Replace("-", "+-").Split('+');            
        string[] monomialNumbers = terms[0].Split('x', '^');
        int index;
        if (monomialNumbers.Length > 2)
        {
            index = int.Parse(monomialNumbers[monomialNumbers.Length - 1]);
        }
        else
        {
            index = monomialNumbers.Length > 1 ? 1 : 0;
        }
        
        var termCoefficients = new double[index + 1];
        foreach (var term in terms)
        {
            monomialNumbers = term.Split('x', '^');
            if (monomialNumbers.Length > 2)
            {
                index = int.Parse(monomialNumbers[monomialNumbers.Length - 1]);
            }
            else
            {
                index = monomialNumbers.Length > 1 ? 1 : 0;
            }
            index = termCoefficients.Length - (index + 1);
            termCoefficients[index] = monomialNumbers[0] == "" ? 1 :  Double.Parse(monomialNumbers[0]);
        }

        return ReduceLeadingZeroCoefficients(termCoefficients);
    }

     private static double[] ReduceLeadingZeroCoefficients(double[] coefficients)
     {
        double[] reducedCoefficients = new double[] { 0 };
        int dim;

        // Check if leading coefficients is zeros.
        for (int i = 0; i < coefficients.Length; i++)
        {
            if (coefficients[i] != 0)
            {
                dim = coefficients.Length - (i + 1);
                reducedCoefficients = new double[dim + 1];
                Array.Copy(coefficients, i, reducedCoefficients, 0, dim + 1);
                break;
            }
        }

        return reducedCoefficients;
     }

     #endregion    
}