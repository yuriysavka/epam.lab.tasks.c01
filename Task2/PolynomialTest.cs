
using System;

class PolynomialTest
{
    static void Main(string[] args)
    {
        // Test #1.
        double[] coefficients = { 1, -10, 0, 15 };
        RealPolynomial p1 = new RealPolynomial(coefficients);
        RealPolynomial p2 = new RealPolynomial("x^2-3x+2");
        RealPolynomial p3 = new RealPolynomial("x^3-10x^2+15");

        Console.WriteLine("p1 = {0}", p1);
        Console.WriteLine("p2 = {0}", p2);

        Console.WriteLine();
        Console.WriteLine("p1 + p2 = {0}", p1.Add(p2));
        Console.WriteLine("p1 - p2 = {0}", p1.Subtract(p2));
        Console.WriteLine("p1 * p2 = {0}", p1.Multiply(p2));

        Console.WriteLine();
        Console.WriteLine("p1 == p2 ? {0}", p1.Equals(p2));
        Console.WriteLine("p1 == p3 ? {0}", p1.Equals(p3));

        double value = 2;
        Console.WriteLine();
        Console.WriteLine("p1({0}) = {1}", value, p1.Evaluate(value));
    }

}