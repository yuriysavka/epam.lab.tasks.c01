
using System;

class Program
{
    static void Main(string[] args)
    {
        RectangleI r1 = new RectangleI(0, 4, 5, 4);
        RectangleI r2 = new RectangleI(4, 5, 4, 3);
        RectangleI r3 = new RectangleI(8, 5, 4, 3);

        Console.WriteLine("r1 = {0}", r1);
        Console.WriteLine("r2 = {0}", r2);
        Console.WriteLine("r3 = {0}", r3);

        Console.WriteLine("\nunion(r1, r2) = {0}", r1.Union(r2));
        Console.WriteLine("r1 intersect r3 ? {0}", r1.Intersects(r3));
        Console.WriteLine("intersect(r1, r2) = {0}", r1.Intersect(r2));

        r1.Resize(1, 4);
        r1.Translate(8, -2);
        Console.WriteLine("\nr1.Resize\nr1.Translate\nr1 = {0}", r1);
        Console.WriteLine("intersect(r1, r3) = {0}", r1.Intersect(r3));
    }
}