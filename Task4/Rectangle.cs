
using System;

class RectangleI
{
    public RectangleI() : this(0, 0, 0, 0)
    { }

    public RectangleI(int width, int height) : this(0, 0, width, height)
    { }

    public RectangleI(RectangleI rectangle)
        : this(rectangle.UpperLeftX, rectangle.UpperLeftY, rectangle.Width, rectangle.Height)
    { }

    public RectangleI(int x, int y, int width, int height)
    {
        this.UpperLeftX = x;
        this.UpperLeftY = y;
        this.Width = width;
        this.Height = height;
    }

    public int UpperLeftX { get; private set; }
    public int UpperLeftY { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }
    public int Left
    {
        get
        {
            return UpperLeftX;
        }
    }
    public int Right
    {
        get
        {
            return UpperLeftX + Width;
        }
    }
    public int Top
    {
        get
        {
            return UpperLeftY;
        }
    }
    public int Bottom
    {
        get
        {
            return UpperLeftY - Height;
        }
    }

    public bool Intersects(RectangleI rectangle)
    {
        if (Right < rectangle.Left || Right < rectangle.Left ||
            Top < rectangle.Bottom || Top < rectangle.Bottom)
        {
            return false;
        }

        return true;
    }

    public RectangleI Intersect(RectangleI rectangle)
    {
        if (!Intersects(rectangle))
        {
            return null;
        }

        int intersectLeft = Math.Max(Left, rectangle.Left);
        int intersecTop = Math.Min(Top, rectangle.Top);
        int intersecRight = Math.Min(Right, rectangle.Right);
        int intersectBottom = Math.Max(Bottom, rectangle.Bottom);

        RectangleI intersecRectangle = new RectangleI
        {
            UpperLeftX = intersectLeft,
            UpperLeftY = intersecTop,
            Width = Math.Abs(intersecRight - intersectLeft),
            Height = Math.Abs(intersecTop - intersectBottom)
        };

        return intersecRectangle;
    }

    public RectangleI Union(RectangleI rectangle)
    {
        int unionLeft = Math.Min(UpperLeftX, rectangle.UpperLeftX);
        int unionTop = Math.Max(UpperLeftY, rectangle.UpperLeftY);
        int unionRight = Math.Max(UpperLeftX + Width, rectangle.UpperLeftX + rectangle.Width);
        int unionBottom = Math.Min(UpperLeftY - Height, rectangle.UpperLeftY - rectangle.Height);

        RectangleI intersecRectangle = new RectangleI
        {
            UpperLeftX = unionLeft,
            UpperLeftY = unionTop,
            Width = Math.Abs(unionRight - UpperLeftX),
            Height = Math.Abs(unionBottom - unionTop)
        };

        return intersecRectangle;
    }

    public void Translate(int offsetX, int offsetY)
    {
        this.UpperLeftX += offsetX;
        this.UpperLeftY += offsetY;
    }

    public void Resize(int width, int height)
    {
        this.Width = width;
        this.Height = height;
    }

    public override string ToString()
    {
        string str = "[(";
        str += UpperLeftX + ", " + UpperLeftY + "), (";
        str += (UpperLeftX + Width) + ", " + (UpperLeftY - Height) + ")]";
        return str;
    }

    public override bool Equals(object obj)
    {
        var rectangle = obj as RectangleI;

        if (rectangle == null)
        {
            return false;
        }

        if (UpperLeftX != rectangle.UpperLeftX || UpperLeftY != rectangle.UpperLeftY ||
            Width != rectangle.Width || Height != rectangle.Height)
        {
            return false;
        }

        return true;
    }

    public override int GetHashCode()
    {
        return this.ToString().GetHashCode();
    }
}