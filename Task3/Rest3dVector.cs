
using System;

class Rest3dVector
{
    private const int dimension = 3;

    private Rest3dVectorArguments[] components =
        new Rest3dVectorArguments[dimension];

    public int Dimension
    {
        get
        {
            return dimension;
        }
    }

    public Rest3dVector()
        : this(Rest3dVectorArguments.Low, Rest3dVectorArguments.Low, Rest3dVectorArguments.Low)
    { }

    public Rest3dVector(Rest3dVectorArguments arg1, Rest3dVectorArguments arg2, Rest3dVectorArguments arg3)
    {
        components[0] = arg1;
        components[1] = arg2;
        components[2] = arg3;
    }

    public Rest3dVector(int arg1, int arg2, int arg3)
        : this(new int[] { arg1, arg2, arg3 })
    { }

    public Rest3dVector(int[] args)
    {
        if ((args == null) || (args.Length != Dimension))
        {
            throw new ArgumentException();
        }

        for (var i = 0; i < Dimension; i++)
        {
            if (!IsValidArgument(args[i]))
            {
                throw new ArgumentException();
            }

            components[i] = (Rest3dVectorArguments)args[i];
        }
    }

    public Rest3dVectorArguments this[int index]
    {
        get
        {
            if (index < 0 || index > (Dimension - 1))
            {
                throw new ArgumentOutOfRangeException();
            }

            return components[index];
        }
    }

    public int ComponentsWithValue(Rest3dVectorArguments arg)
    {
        int count = 0;

        for (int i = 0; i < Dimension; i++)
        {
            if (arg == components[i])
            {
                count++;
            }
        }

        return count;
    }

    public int ComponentsWithValue(int arg)
    {
        int count = 0;

        if (!IsValidArgument(arg))
        {
            throw new ArgumentException();
        }

        for (int i = 0; i < Dimension; i++)
        {
            if (arg == (int)components[i])
            {
                count++;
            }
        }

        return count;
    }

    public bool IsOrtogonal(Rest3dVector vector)
    {
        return DotProduct(vector) == 0;
    }

    public Rest3dVector Intersect(Rest3dVector vector)
    {
        if (IsOrtogonal(vector))
        {
            return null;
        }

        Rest3dVector resultVector = new Rest3dVector(
            IntersectArgumentRule(this[0], vector[0]),
            IntersectArgumentRule(this[1], vector[1]),
            IntersectArgumentRule(this[2], vector[2])
            );

        return resultVector;
    }

    public override string ToString()
    {
        string str = "";
        str = "(" + (int)components[0] + ", " + (int)components[1] + "," + 
            (int)components[2] + ")";

        return str;
    }

    public enum Rest3dVectorArguments
    {
        Low = 0,
        Middle = 1,
        High = 2
    }

    #region Helpers

    private static bool IsValidArgument(int arg)
    {
        var values = Enum.GetValues(typeof(Rest3dVectorArguments));

        bool isValid = Array.Exists<Rest3dVectorArguments>(values as Rest3dVectorArguments[],
        i =>
        {
            foreach (var value in values)
            {
                if (arg == (int)value)
                {
                    return true;
                }
            }
            return false;
        });

        return isValid;
    }

    private static Rest3dVectorArguments IntersectArgumentRule(Rest3dVectorArguments arg1, Rest3dVectorArguments arg2)
    {
        if (arg1 == Rest3dVectorArguments.Low || arg1 == Rest3dVectorArguments.Low)
        {
            return Rest3dVectorArguments.Low;
        }
        else if (arg1 == Rest3dVectorArguments.Middle || arg2 == Rest3dVectorArguments.Middle)
        {
            return Rest3dVectorArguments.Middle;
        }

        return Rest3dVectorArguments.High;
    }

    private int DotProduct(Rest3dVector vector)
    {
        int dotProduct = 0;

        for (int i = 0; i < Dimension; i++)
        {
            dotProduct += (int)this[i] * (int)vector[i];
        }

        return dotProduct;
    }

    #endregion
}