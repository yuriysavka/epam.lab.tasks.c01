
using System;

class VectorTest
{
    static void Main(string[] args)
    {
        Rest3dVector vectorA = new Rest3dVector(1, 2, 2);
        Rest3dVector vectorB = new Rest3dVector(0, 1, 2);

        Console.WriteLine("vectorA : \n\t{0}", vectorA);
        Console.WriteLine("vectorB : \n\t{0}", vectorB);
        Console.WriteLine("Components with value {0} in vectorA : \n\t{0}",
            vectorA.ComponentsWithValue(2));
        Console.WriteLine("Intersection of vectorA vs vectorB: \n\t{0}", vectorA.Intersect(vectorB));
    }
}