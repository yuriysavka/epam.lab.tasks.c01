
using System;

class SquareMatrix
{
    private Matrix matrix;

    public int Dimension
    {
        get
        {
            return matrix.Rows;
        }
    }

    public SquareMatrix()
    {
        matrix = new Matrix();
    }

    public SquareMatrix(int dimension)
    {
        matrix = new Matrix(dimension, dimension);
    }

    public SquareMatrix(double[,] array)
    {
        if (array.GetLength(0) < 1 || array.GetLength(1) < 1 ||
            array.GetLength(0) != array.GetLength(1))
        {
            throw new ArgumentException();
        }

        matrix = new Matrix(array.Clone() as double[,]);
    }

    public double MinorOfOrder(int order)
    {
        if ((order < 0) || (order > Dimension))
        {
            throw new ArgumentOutOfRangeException();
        }

        // Minor of order 0 is defined to be 1.
        if (order == 0)
        {
            return 1;
        }

        int[] selectedIndexes = new int[order];

        for (int i = 0; i < order; i++)
        {
            selectedIndexes[i] = i;
        }

        return matrix.Minor(selectedIndexes, selectedIndexes);
    }

    public double Cofactor(int rowIndex, int columnIndex)
    {
        if (rowIndex < 0 || columnIndex < 0 ||
            rowIndex > (Dimension - 1) || columnIndex > (Dimension - 1))
        {
            throw new ArgumentOutOfRangeException();
        }

        int sign = ((rowIndex + columnIndex) % 2) == 0 ? 1 : -1;

        return sign * Minor(rowIndex, columnIndex);
    }

    public double Minor(int rowIndex, int columnIndex)
    {
        if (rowIndex < 0 || columnIndex < 0 ||
            rowIndex > (Dimension - 1) || columnIndex > (Dimension - 1))
        {
            throw new ArgumentOutOfRangeException();
        }

        return Matrix.Minor(matrix.RemoveRowCol(rowIndex, columnIndex));
    }

    public double AdjunctMinor(int toMinorOrder)
    {
        if ((toMinorOrder < 0) || (toMinorOrder > Dimension))
        {
            throw new ArgumentOutOfRangeException();
        }

        var selectedIndexes = new int[Dimension - toMinorOrder];

        for (int i = 0; i < selectedIndexes.Length; i++)
        {
            selectedIndexes[i] = Dimension - (i + 1);
        }

        return matrix.Minor(selectedIndexes, selectedIndexes);
    }

    public SquareMatrix GetMinors()
    {
        var array = new double[Dimension, Dimension];

        for (int i = 0; i < Dimension; i++)
        {
            for (int j = 0; j < Dimension; j++)
            {
                array[i, j] = Minor(i, j);
            }
        }

        return new SquareMatrix(array);
    }

    public SquareMatrix GetCofactors()
    {
        var array = new double[Dimension, Dimension];

        for (int i = 0; i < Dimension; i++)
        {
            for (int j = 0; j < Dimension; j++)
            {
                array[i, j] = Cofactor(i, j);
            }
        }

        return new SquareMatrix(array);
    }

    public override string ToString()
    {
        return matrix.ToString();
    }
}