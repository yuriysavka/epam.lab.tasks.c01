
using System;

class MatrixTest
{
    static void Main(string[] args)
    {
        Matrix aMatrix = new Matrix(new double[,] {
                { 4, 2, 4, 8 },
                { 1, 5, 3, 7 },
                { 1, -1, 3, 2 },
            });

        SquareMatrix bMatrix = new SquareMatrix(new double[,] {
                { 1, 2, 4 },
                { 1, 5, 3 },
                { 0, 5, 7 }
            });

        Console.Write("Test 1............................");
        Console.WriteLine("\naMatrix: \n{0}", aMatrix);

        int[] selectedRows = new int[] { 1, 2 };
        int[] selectedColumns = new int[] { 0, 3 };
        Console.WriteLine("Minor for selected rows/columns in aMatrix: \n\t{0}",
            aMatrix.Minor(selectedRows, selectedColumns));

        Console.Write("Test 2............................");
        Console.WriteLine("\nbMatrix: \n{0}", bMatrix);
        Console.WriteLine("Determinant of bMatrix: \n\t{0}",
            bMatrix.MinorOfOrder(bMatrix.Dimension));
        Console.WriteLine("Minors of bMatrix: \n{0}", bMatrix.GetMinors());
        Console.WriteLine("Cofactors of bMatrix: \n{0}", bMatrix.GetCofactors());
        Console.WriteLine("Minor of {0}-th order of bMatrix: \n\t{1}", 2,
            bMatrix.MinorOfOrder(2));
        Console.WriteLine("Adjunct minor to {0}-th order minor of bMatrix: \n\t{1}", 2,
            bMatrix.AdjunctMinor(2));
    }
}