
using System;
using System.Text;

class Matrix
{
    private double[,] matrix;

    public int Rows
    {
        get
        {
            return matrix.GetLength(0);
        }
    }

    public int Columns
    {
        get
        {
            return matrix.GetLength(1);
        }
    }

    public double this[int i, int j]
    {
        get
        {
            return matrix[i, j];
        }
        set
        {
            matrix[i, j] = value;
        }
    }

    public Matrix() : this(1, 1)
    { }

    public Matrix(double[,] array)
    {
        if (array.GetLength(0) < 1 || array.GetLength(1) < 1)
        {
            throw new ArgumentException("Matrix dimensions if less than 1.");
        }

        matrix = array.Clone() as double[,];
    }

    public Matrix(int rowsCount, int columnsCount)
    {
        if (rowsCount < 1 || columnsCount < 1)
            throw new ArgumentException("Matrix dimensions if less than 1.");

        matrix = new double[rowsCount, columnsCount];
    }

    public static double Minor(Matrix squareMatrix)
    {
        int order = squareMatrix.Rows;

        if (squareMatrix.Rows != squareMatrix.Columns)
        {
            throw new ArgumentException();
        }

        double[,] array = squareMatrix.ToArray();

        if (order == 1)
        {
            return squareMatrix[0, 0];
        }

        double det = 0;
        int sign = 1;

        for (int j = 0; j < order; j++)
        {
            sign = (j % 2) == 0 ? 1 : -1;
            det += sign * squareMatrix[0, j] * Minor(squareMatrix.RemoveRowCol(0, j));
        }

        return det;
    }

    public double Minor(int[] selectedRowsIndexes, int[] selectedColumnsIndexes)
    {
        if (selectedRowsIndexes.Length != selectedRowsIndexes.Length)
        {
            throw new ArgumentException("Rows and columns must be equal.");
        }

        // Get square matrix via selected indexes.
        Matrix subMatrix = Submatrix(selectedRowsIndexes, selectedColumnsIndexes);

        return Minor(subMatrix);
    }

    public Matrix RemoveRowCol(int rowIndexToRemove, int columnIndexToRemove)
    {
        double[,] result = new double[Rows - 1, Columns - 1];

        int k = 0, l = 0;
        for (int i = 0; i < Rows; i++)
        {
            if (i == rowIndexToRemove)
            {
                continue;
            }

            for (int j = 0; j < Columns; j++)
            {
                if (j == columnIndexToRemove)
                {
                    continue;
                }

                result[l, k] = matrix[i, j];

                k = (k + 1) % (Rows - 1);
                if (k == 0)
                {
                    l++;
                }
            }
        }

        return new Matrix(result);
    }

    public double[,] ToArray()
    {
        double[,] array = new double[Rows, Columns];

        for (int i = 0; i < Rows; i++)
        {
            for (int j = 0; j < Columns; j++)
            {
                array[i, j] = matrix[i, j];
            }
        }

        return array;
    }

    public override string ToString()
    {
        StringBuilder sbResult = new StringBuilder();

        for (int i = 0; i < Rows; i++)
        {
            for (int j = 0; j < Columns; j++)
            {
                sbResult.Append("\t");
                sbResult.Append(this[i, j]);
            }

            sbResult.Append("\n");
        }

        return sbResult.ToString();
    }

    #region Helpers

    private Matrix Submatrix(int[] selectedRowsIndexes, int[] selectedColumnsIndexes)
    {
        int rows = selectedRowsIndexes.Length;
        int columns = selectedColumnsIndexes.Length;

        if ((rows < 1) || (columns < 1) || (rows > Rows) || (columns > Columns))
        {
            throw new ArgumentException();
        }

        double[,] subArray = new double[rows, columns];

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                subArray[i, j] = matrix[selectedRowsIndexes[i], selectedColumnsIndexes[j]];
            }
        }

        return new Matrix(subArray);
    }

    #endregion
}